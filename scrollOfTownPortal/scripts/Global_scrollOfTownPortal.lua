local core = require('openmw.core') 
local util = require('openmw.util')
local types = require('openmw.types')
local world = require('openmw.world')
local async = require('openmw.async')
local aux_util = require('openmw_aux.util')
local interfaces = require('openmw.interfaces')
local animation = require('openmw.animation')

--TODO: make player face the correct way after they teleport

local homePortal = nil
local awayPortal = nil

local trans = util.transform
local homeCoords = {
	hlaalu = {cell = "Odai Plateau", coords = util.vector3(-35070.46875, -36171.94921875, 1881)},
	redoran = {cell = "Bal Isra", coords = util.vector3(-35249.917969, 78964.148438, 1735.710083)}, 
	telvanni = {cell = "Uvirith's Grave", coords = util.vector3(87322.8515625, 10778.3525390625, 1911.9886474609375)}}


local function d_message(msg)
	if not debug then return end

	ui.showMessage(tostring(msg))
end

local function d_print(fname, msg)
	if not debug then return end

	if fname == nil then
		fname = "\x1b[35mnil"
	end

	if msg == nil then
		msg = "\x1b[35mnil"
	end

	print("\n\t\x1b[33;3m" .. tostring(fname) .. "\n\t\t\x1b[33;3m" .. tostring(msg) .. "\n\x1b[39m")
end 


local function getHomeCoords(player)

	local gb_vars = world.mwscript.getGlobalVariables(player)

	local faction = nil
	local playerFactions = types.NPC.getFactions(player)


	if types.NPC.getFactionRank(player, "hlaalu") > 0 then faction = "hlaalu" end
	if types.NPC.getFactionRank(player, "redoran") > 0 then faction = "redoran" end
	if types.NPC.getFactionRank(player, "telvanni") > 0 then faction = "telvanni" end


	if faction == nil then return nil end

	return homeCoords[faction]
end


local function destroyPortal(portal)
	local effect = core.magic.effects.records[core.magic.EFFECT_TYPE.Sanctuary]
	core.vfx.spawn(effect.castStatic, portal.position)
--	core.vfx.spawn(effect.areaStatic, portal.position)
--	core.vfx.spawn(effect.boltStatic, portal.position)

	core.sound.playSound3d("destruction hit", portal, {timeOffset = 0.3, volume = 0.25, loop = false, pitch = 2.5})
	portal:remove()
end


local function cb_town_portal(portal, player)

	local hcoords = getHomeCoords(player)
	local trans = util.transform

	if hcoords == nil then print("cb_ hcoords was nil") return end

	local effect = core.magic.effects.records[core.magic.EFFECT_TYPE.Dispel]
	core.vfx.spawn(effect.hitStatic, player.position)

	async:newUnsavableSimulationTimer(0.8, function()  

		core.sound.playSound3d("Thunder2", player, {timeOffset = 0.3, volume = 0.25, loop = false, pitch = 2.5})
		core.sound.playSound3d("Thunder2", player, {timeOffset = 0.35, volume = 0.3, loop = false, pitch = 1})


		if portal == homePortal then

			local fromActorSpace = trans.move(awayPortal.position) * trans.rotateZ(awayPortal.rotation:getYaw())
			local posInFrontOfPortal = fromActorSpace * util.vector3(0, 80, 0)

			player:teleport(awayPortal.cell.name, posInFrontOfPortal)

			destroyPortal(portal)
			homePortal = nil

			destroyPortal(awayPortal)
			awayPortal = nil
		else
			local fromActorSpace = trans.move(homePortal.position) * trans.rotateZ(1)
			local posInFrontOfPortal = fromActorSpace * util.vector3(0, 80, 0)

			player:teleport(homePortal.cell.name, posInFrontOfPortal)
		end
	end)
end



local function cb_cast_town_portal(player)
--local function cb_cast_home_portal(scroll, player, options)

	local hcoords = getHomeCoords(player)
	
	if hcoords == nil then print("cb_cast hcoords was nil") return false end

--	if player.cell.name == hcoords.cell then return false end

	if homePortal ~= nil then
		destroyPortal(homePortal)
	end

	if awayPortal ~= nil then
		destroyPortal(awayPortal)
	end


	local fromActorSpace = trans.move(player.position) * trans.rotateZ(player.rotation:getYaw())
	local posInFrontOfPlayer = fromActorSpace * util.vector3(0, 80, 0)


	async:newUnsavableSimulationTimer(1.25, function()  
		local effect = core.magic.effects.records[core.magic.EFFECT_TYPE.SummonBonewalker]
		core.vfx.spawn(effect.castStatic, posInFrontOfPlayer, {scale = 1.5})

		awayPortal = world.createObject("TownPortalShrine", 1)
		awayPortal:teleport(player.cell, posInFrontOfPlayer)
		awayPortal:setScale(2)
		interfaces.Activation.addHandlerForObject(awayPortal, cb_town_portal)


--		print("coords: " .. tostring(hcoords.rotation))

		core.vfx.spawn(effect.castStatic, hcoords.coords, {scale = 2})

		homePortal = world.createObject("TownPortalShrine", 1)
		homePortal:teleport(hcoords.cell, hcoords.coords)
		homePortal:setScale(2)
		interfaces.Activation.addHandlerForObject(homePortal, cb_town_portal)
	end)

	--let the scroll cast its built in effect to play the animation and the vfx/sound etc
	--TODO: do this manually?
	return true
end

local function createRecipes()
	if interfaces.gnounc_transmuteCube ~= nil then
		interfaces.gnounc_transmuteCube.registerRecipes({
			{product = "sc_townPortal", ingredients = {{id_contains = "_soulgem_", quantity = 1}, {id = "sc_almsiviintervention", quantity = 1}, {id = "sc_divineintervention", quantity = 1}}, description = "Scroll of Town Portal<BR>    1 Scroll of Almsivi Intervention<BR>    1 Scrolls of Divine Intervention<BR>    1 Soul Gem (any quality)"}
		})
	else
		print("--------cube recipe not registered. cube mod not installed-----------")
	end
end

local function onInit()
	createRecipes()
end

local function onLoad()
	createRecipes()
end

local function onPlayerAdded(player)
	--this wont work with quickkeys. think about changing how we do this
--	interfaces.ItemUsage.addHandlerForObject(tp, cb_cast_town_portal)

--	local tp = world.createObject('sc_townPortal', 5)
--	tp:moveInto(types.Actor.inventory(player))
end


--dont forget to save and load this?
local function register_town(faction, cellname, coords)
	homeCoords[faction] = {cell = cellname, coords = coords}
end

return {
	interfaceName = "gnounc_town_portal",
	interface = {
		version = 1,
		register_town = register_town,
	},
	eventHandlers = {
		event_town_portal = cb_cast_town_portal,
	},
	engineHandlers = {
		onInit = onInit,
		onLoad = onLoad,
		onUpdate = onUpdate,
		onPlayerAdded = onPlayerAdded
	} 
}