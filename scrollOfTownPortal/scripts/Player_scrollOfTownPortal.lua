local core = require('openmw.core') 
local types = require('openmw.types')
local input = require('openmw.input')
local self = require('openmw.self')
local ui = require('openmw.ui')
local time = require('openmw_aux.time')
local interfaces = require('openmw.interfaces')




local function d_message(msg)
	if not debug then return end

	ui.showMessage(tostring(msg))
end

local function d_print(fname, msg)
	if not debug then return end

	if fname == nil then
		fname = "\x1b[35mnil"
	end

	if msg == nil then
		msg = "\x1b[35mnil"
	end

	print("\n\t\x1b[33;3m" .. tostring(fname) .. "\n\t\t\x1b[33;3m" .. tostring(msg) .. "\n\x1b[39m")
end 

local function onInputAction(action)

	if action ~= input.ACTION.Use then return end

	if types.Actor.getStance(self) ~= types.Actor.STANCE.Spell then return end 

	local eq_scroll = types.Actor.getSelectedEnchantedItem(self)

	if eq_scroll == nil then return end 

	if eq_scroll.recordId ~= "sc_townportal" then return end

	core.sendGlobalEvent("event_town_portal", self)
end

return {
	eventHandlers = {
	},
	engineHandlers = {
		onInputAction = onInputAction
	} 
}