# openmw_townPortal

## Requires OpenMW 0.49 or above

## Youtube Demonstration
n/a

## Installation
--todo

## To Use:
1. In the mod settings page, bind keys for open and transmute
2. Find or craft (if you have the dwemer transmutation mod as well) some town portal scrolls 
3. Cast scroll, and enter the portal!

## TODO:
1. None.

## Known Bugs:
None

## License
--TODO: say how it is licensed.

